type CarYear = number;
type CarType = string;
type CarModel = string;

type Car = {
    year: CarYear,
    type: CarType,
    model: CarModel,
}

const carYear: CarYear = 2001;
const CarType: CarType = "Toyota";
const CarModel: CarModel = "Corolla";

const car1 : Car = {
    year: 2001,
    type: "Nissan",
    model: "xxx",
}

console.log(car1);